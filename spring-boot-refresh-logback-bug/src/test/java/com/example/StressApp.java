package com.example;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.springframework.web.client.RestTemplate;

public class StressApp {

   private static final RestTemplate restTemplate = new RestTemplate();
 
   
   @Test
   public static void main(String[] args) throws InterruptedException{
      int numberOfThreads = 100;
      ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
      
      for (int i=0;i<numberOfThreads;i++) {
         executor.execute(new LoadTestRunnable());
      }
      executor.shutdown();
      executor.awaitTermination(1, TimeUnit.DAYS);
      
   }
   
   static class LoadTestRunnable implements Runnable {
      
      @Override
      public void run() {
         // I know this is all bad but it's just a test :-)
         while(true){
            System.out.println("dave");
            restTemplate.getForObject("http://localhost:8080/someurl", String.class);
            try {
               Thread.sleep(100);
            } catch (InterruptedException e) {

            }
         }
      }
   };
   
}
