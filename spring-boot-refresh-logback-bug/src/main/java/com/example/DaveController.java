package com.example;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DaveController {
   
   private static final Logger logger = LoggerFactory.getLogger(DaveController.class);

   @RequestMapping("/someurl")
   public List<String> getSomething(){
      logger.debug("Enter getSomething");
      List<String> response = Arrays.asList("one","two","three","...");
      logger.debug("Exit getSomething");
      return response;
   }
   
}
